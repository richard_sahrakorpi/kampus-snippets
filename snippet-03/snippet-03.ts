// snippet from src/app/shared/effects/teas-test.effects.spec.ts
describe('Snippet02Effects', () => {
  let actions$: Observable<teasTestsActions.TestsActionsUnion>;
  let spectator: SpectatorService<Snippet02Effects>;
  let store$: MockStore;
  let effects: Snippet02Effects;
  let loadStatus: MemoizedSelector<State, LoadingStateConstants>;

  const createService = createServiceFactory({
    service: Snippet02Effects,
    imports: [TranslateModule.forRoot()],
    providers: [
      provideMockActions(() => actions$),
      mockProvider(AssignmentsService),
      provideMockStore({ initialState: { ...testStates } }),
    ],
  });

  beforeEach(() => {
    spectator = createService();
    store$ = spectator.inject(MockStore);
    loadStatus = store$.overrideSelector(selectTestLoadStatus, 'CLEARED');
    effects = spectator.service;
  });

  describe('effect1$', () => {
    it.each(['CLEARED', 'ERROR'])('should ... when status is %s', (status) => {
      Scheduler.get().run(({ hot, expectObservable }) => {
        actions$ = hot('-a', {
          a: teasTestsActions.loadAssignmentsOnce(),
        });

        loadStatus.setResult(status);
        store$.refreshState();

        expectObservable(effects.effect1$).toBe('-a', {
          a: teasTestsActions.loadAssignments(),
        });
      });
    });

    it.each(['FETCHED', 'FETCHING', 'UPDATING'])('should ... when status is %s', (status) => {
      Scheduler.get().run(({ hot, expectObservable }) => {
        actions$ = hot('-a', {
          a: teasTestsActions.loadAssignmentsOnce(),
        });

        loadStatus.setResult(status);
        store$.refreshState();

        expectObservable(effects.effect1$).toBe('-', {});
      });
    });
  });

  describe('effect2$', () => {
    it('1', () => {
      Scheduler.get().run(({ hot, cold, expectObservable }) => {
        actions$ = hot('-a', {
          a: teasTestsActions.loadAssignments(),
        });

        (
          spectator.inject(AssignmentsService).getAssignments as unknown as jest.MockInstance<
            Observable<AssignmentListDto>,
            Parameters<AssignmentsService['getAssignments']>
          >
        ).mockReturnValue(
          cold('--a', {
            a: MOCK_ASSIGNMENT_LIST,
          })
        );

        expectObservable(effects.effect2$).toBe('---a', {
          a: teasTestsActions.loadAssignmentsSuccess(MOCK_ASSIGNMENT_LIST),
        });
      });
    });

    it('2', () => {
      Scheduler.get().run(({ hot, cold, expectObservable }) => {
        actions$ = hot('-a', {
          a: teasTestsActions.loadAssignments(),
        });

        spectator.inject(AssignmentsService).getAssignments.mockReturnValue(cold('--#', null, new Error()));

        expectObservable(effects.effect2$).toBe('---a', {
          a: teasTestsActions.loadAssignmentsFailure(),
        });
      });
    });
  });
});
