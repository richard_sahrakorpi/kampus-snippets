// snippet from libs/component-library/buttons/src/lib/button/button.component.spec.ts

describe('ButtonComponent', () => {
  let component: ButtonComponent;
  let spectator: Spectator<ButtonComponent>;

  const createComponent = createComponentFactory({
    component: ButtonComponent,
    imports: [NoopAnimationsModule],
  });

  beforeEach(() => {
    spectator = createComponent();
    component = spectator.component;
    spectator.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display loader', () => {
    expect(spectator.query('.loader')).not.toExist();

    spectator.setInput({ loading: true });
    spectator.detectChanges();
    expect(spectator.query('.loader')).toExist();

    spectator.setInput({ loading: false });
    spectator.detectChanges();
    expect(spectator.query('.loader')).not.toExist();
  });
});
