// snippet from src/app/test/pages/new-test/new-test.component.ts
import { Component, ChangeDetectionStrategy, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { combineLatest, merge, defer, of } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  templateUrl: './new-test.component.html',
  styleUrls: ['./new-test.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class Snippet01Component implements OnDestroy, OnInit {
  constructor(private store$: Store, private fb: FormBuilder) {}

  filterFormGroup = this.fb.group({
    searchInput: [''],
    feed: [null],
  });

  readonly outlines$ = this.store$.select(selectOutlines);

  valueChanges$ = merge(
    defer(() => of(this.filterFormGroup.value)),
    this.filterFormGroup.valueChanges
  );

  filteredOutlines$ = combineLatest([this.valueChanges$, this.outlines$]).pipe(
    map(([formValue, outlines]) => this.getFilteredOutLines(outlines, formValue)),
    shareReplay({ bufferSize: 1, refCount: true })
  );

  getFilteredOutLines(outlines, formValue) {
    return outlines
      .filter((outline) => formValue.feed?.productIds.includes(outline.metadata.productId))
      .filter((outline) => outline.title.toLowerCase().includes(formValue.searchInput.toLowerCase()));
  }
}
