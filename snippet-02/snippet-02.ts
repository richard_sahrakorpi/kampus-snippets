// snippet from src/app/shared/effects/teas-test.effects.ts
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, exhaustMap, filter, map } from 'rxjs/operators';

@Injectable()
export class Snippet02Effects {
  constructor(
    private actions$: Actions<teasTestsActions.TestsActionsUnion>,
    private store$: Store,
    private assignmentsService: AssignmentsService
  ) {}

  effect1$ = createEffect(() =>
    this.actions$.pipe(
      ofType(teasTestsActions.loadAssignmentsOnce),
      concatLatestFrom(() => this.store$.select(selectTestLoadStatus)),
      filter(
        ([, status]) =>
          !["FETCHED", "FETCHING", "UPDATING"].includes(
            status
          )
      ),
      map(() => teasTestsActions.loadAssignments())
    )
  );

  effect2$ = createEffect(() =>
    this.actions$.pipe(
      ofType(teasTestsActions.loadAssignments),
      exhaustMap(() =>
        this.assignmentsService.getAssignments().pipe(
          map((assignmentList) => teasTestsActions.loadAssignmentsSuccess({ assignments: assignmentList })),
          catchError(() => of(teasTestsActions.loadAssignmentsFailure()))
        )
      )
    )
  );
}